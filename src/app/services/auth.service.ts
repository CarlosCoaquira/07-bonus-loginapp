import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UsuarioModel } from '../models/usuario.model';

import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = 'https://identitytoolkit.googleapis.com/v1/accounts:';
  // tokem generado con carloscoaquiramalaver@gmail.com
  private apiKey = 'AIzaSyCMS9E3nzY6SxHd8Tj_6A5LjgVhc5A2GHk';

  userToken: string;

  // Crear nuevos usuarios
  // https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]

  // Login
  // https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]


  constructor( private http: HttpClient) {

    this.leerToken();
   }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('email');
  }

  login(usuario: UsuarioModel) {
    const authData = {
      // email: usuario.email,
      // password: usuario.password,
      ...usuario, // es equivalente a enviar por separado los campos aunque este metodo incluye todo los campos del modelo
      returnSecureToken: true
    };

    return this.http.post(
      `${ this.url }signInWithPassword?key=${ this.apiKey }`,
      authData
    ).pipe(
      map( resp => {
        // console.log("entro a map");
        this.guardarToken(resp['idToken']);
        return resp;
      } )
    );

    // el campo idToken es el que se debe utilizar
  }

  nuevoUsuario(usuario: UsuarioModel) {

    const authData = {
      // email: usuario.email,
      // password: usuario.password,
      ...usuario, // es equivalente a enviar por separado los campos aunque este metodo incluye todo los campos del modelo
      returnSecureToken: true
    };

    return this.http.post(
      `${ this.url }signUp?key=${ this.apiKey }`,
      authData
    ).pipe(
      map( resp => {
        // console.log("entro a map");
        this.guardarToken(resp['idToken']);
        return resp;
      } )
    );

  }

  private guardarToken(idToken: string) {
    this.userToken = idToken;
    localStorage.setItem('token', idToken);

    let hoy = new Date();
    hoy.setSeconds(3600); // valor en duro "asumido"

    localStorage.setItem('expira', hoy.getTime().toString());
  }

  leerToken() {
    if (localStorage.getItem('token')) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }
    return this.userToken;
  }

  estaAutenticado(): boolean {

    if (this.userToken.length > 2) {
      return false;
    }

    const expira = Number(localStorage.getItem('expira'));
    const expiraDate = new Date();
    expiraDate.setTime(expira);

    if (expiraDate > new Date()) {
      return true;
    } else {
      return false;
    }

    // return this.userToken.length > 2;
  }
}
